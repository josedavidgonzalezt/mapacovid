import React, {Fragment,useState} from 'react';
import {Map,TileLayer} from 'react-leaflet';
import EdificiosGCBA from './components/EdificiosGCBA';
import Cesac from './components/Cesac';
import Unidadesfebriles from './components/Unidadesfebriles';
import HospitalesGCBA from './components/HospitalesGCBA';
import VacunatoriosAMayores from './components/VacunatoriosAMayores';
import Seleccione from './components/Seleccion';
import Miubicacion from './components/Miubicacion';
// Instalamos npm install leaflet.locatecontrol (MiUbicacion)
//npm install react-leaflet (para el mapa)
//npm install leaflet (para el icono)
function App() {
    
  // Cambia el modo del Mapa
  const [mapa,cambiarMapa]=useState(true);
  //Oculta o muestra los puntos
  const [mostrarEdificio,ocultarEdificio]=useState(false);
  const [mostrarUnidadesFebriles, ocultarUnidadesFebriles]= useState(false);
  const [mostrarCesac, ocultarCesac]= useState(false);
  const [mostrarEdificioGCBA, ocultarEdificioGCBA]= useState(false);
  const [mostrarVacunatorios, ocultarVacunatorios]= useState(false);

  return ( 
    <Fragment>
        <div className="barra">
            <div>
                <img className="ba" alt="logoba" src="https://badesdeadentro.gob.ar/sites/default/files/inline-images/Logo%20para%20web%2020190308-01.png"/>
            </div>
            <div>
                <Seleccione
                    mapa={mapa}
                    cambiarMapa={cambiarMapa}
                    ocultarEdificio={ocultarEdificio}
                    mostrarEdificio={mostrarEdificio}
                    mostrarUnidadesFebriles={mostrarUnidadesFebriles}
                    ocultarUnidadesFebriles={ocultarUnidadesFebriles}
                    mostrarCesac={mostrarCesac}
                    ocultarCesac={ocultarCesac}
                    mostrarEdificioGCBA={mostrarEdificioGCBA}
                    ocultarEdificioGCBA={ocultarEdificioGCBA}
                    mostrarVacunatorios={mostrarVacunatorios}
                    ocultarVacunatorios={ocultarVacunatorios}
                />
             </div>
        </div>
                
    
            <Map center={[-34.6131500, -58.3772300]} zoom={11.5} >
                {mapa 
                
                    ? <TileLayer
                        attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a>|BADA contributors'
                        url='https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png'
                        //'https://tiles.stadiamaps.com/tiles/alidade_smooth_dark/{z}/{x}/{y}{r}.png MODO OSCURO
                    />
                    : <TileLayer
                        attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a>|BADA contributors'
                        url='https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}'/>
                }

                {mostrarEdificio ?<HospitalesGCBA/>:null}
                {mostrarUnidadesFebriles ?<Unidadesfebriles/> :null}
                {mostrarCesac ?<Cesac/> :null}
                {mostrarEdificioGCBA ?<EdificiosGCBA/> :null}
                {mostrarVacunatorios ?<VacunatoriosAMayores/> :null}/>
                <Miubicacion/>
            </Map>
</Fragment>

 );
} 

export default App;
