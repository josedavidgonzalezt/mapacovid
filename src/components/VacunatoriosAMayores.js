import React,{Fragment,useState} from 'react'; 
import {Marker, Popup} from 'react-leaflet';
import {icoVacunatorios} from './Iconos';
import * as Vacunatorios from '../data/vacunatoriosadultosmayores.json';

const VacunatoriosAMayores = () => {
    //Activa o desactiva el Popup
    const [activePonitVacunatorio, setActivePonitVacunatorio] = useState(null);
    return ( 
<Fragment>
        {Vacunatorios.features.map(vacunatorio=>(
            <Marker
                icon={icoVacunatorios} 
                key={vacunatorio.properties.id}
                position={[
                    vacunatorio.geometry.coordinates[1],
                    vacunatorio.geometry.coordinates[0]
                ]}
                onclick={()=>{ setActivePonitVacunatorio(vacunatorio);}}
            />
        ))}
        {activePonitVacunatorio && 
            <Popup 
                position={[
                    activePonitVacunatorio.geometry.coordinates[1],
                    activePonitVacunatorio.geometry.coordinates[0]
                ]}
                onClose={()=>{setActivePonitVacunatorio(null);}}
                >
                
                <div className="mypopup">
                    <h5>{activePonitVacunatorio.properties.nombre}</h5>
                     <p>Barrio: {activePonitVacunatorio.properties.barrio}</p>
                     <p>Tipo: {activePonitVacunatorio.properties.tipo}</p>
                     <p>Dirección: {activePonitVacunatorio.properties.domicilio}</p>
                </div>
            </Popup>
        }
</Fragment>
     );
}
 
export default VacunatoriosAMayores;