import React,{Fragment,useState} from 'react'; 
import {Marker, Popup} from 'react-leaflet';
import {icoEdificiosGCBA} from './Iconos';
import * as Edificios from '../data/edificiospublicosgcba.json';

const EdificiosGCBA = () => {
    //Activa o desactiva el Popup
    const [activePonitEdificios, setActivePonitEdificios] = useState(null);
 return ( 
<Fragment>
        {Edificios.features.map(edificio=>(
            <Marker
                icon={icoEdificiosGCBA} 
                key={edificio.properties.id}
                position={[
                    edificio.geometry.coordinates[1],
                    edificio.geometry.coordinates[0]
                ]}
                onclick={()=>{ setActivePonitEdificios(edificio);}}
            />
        ))}
        {activePonitEdificios && 
            <Popup 
                position={[
                    activePonitEdificios.geometry.coordinates[1],
                    activePonitEdificios.geometry.coordinates[0]
                ]}
                onClose={()=>{setActivePonitEdificios(null);}}
                >
                
                <div className="mypopup">
                    <h5>{activePonitEdificios.properties.Nombre}</h5>
                </div>
            </Popup>
        }
</Fragment>
     );
}
 
export default EdificiosGCBA;