import React,{Fragment,useState} from 'react';
import {Marker, Popup} from 'react-leaflet';
import {icoUfu} from './Iconos';
import * as Ufus from '../data/UFUS.json';

const Unidadesfebriles = () => {
    //Activa o desactiva el Popup
    const [activePointUfus, setActivePointUfus] = useState(null);
    return ( 
        <Fragment>
            {Ufus.features.map(ufu=>(
                <Marker
                    icon={icoUfu} 

                    position={[
                        ufu.geometry.coordinates[1],
                        ufu.geometry.coordinates[0]
                    ]}
                    onclick={()=>{ setActivePointUfus(ufu);}}
                />
            ))}
            {activePointUfus && 
                <Popup 
                    position={[
                        activePointUfus.geometry.coordinates[1],
                        activePointUfus.geometry.coordinates[0]
                    ]}
                    onClose={()=>{setActivePointUfus(null);}}
                    >
                    
                    <div className="mypopup">
                        <h5>{activePointUfus.properties.nombre}</h5>
                        <p>Barrio: {activePointUfus.properties.barrio}</p>
                        <p>Dirección: {activePointUfus.properties.direccion_normalizada}</p>
                    </div>
                </Popup>
            }
        </Fragment>
     );
}
 
export default Unidadesfebriles;