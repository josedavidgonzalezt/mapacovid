import React, {Fragment} from 'react';

const Seleccione = ({mapa,cambiarMapa,ocultarEdificio,mostrarEdificio, mostrarUnidadesFebriles,ocultarUnidadesFebriles,mostrarCesac,ocultarCesac,mostrarEdificioGCBA,ocultarEdificioGCBA,mostrarVacunatorios,ocultarVacunatorios}) => {
    
    
    return ( 
        <Fragment>
                <label className="punto">
                    <span className="spanimg"><img className="imagen" alt="Hospitales GCBA" src="https://badesdeadentro.gob.ar/sites/default/files/2020-05/Hospitales.png"/></span><span className="textoinput">Hospitales GCBA</span>
                    {
                        mostrarEdificio 
                        ?<input className="inputcheck" type="checkbox" checked onClick={()=>ocultarEdificio(false)}/>
                        : <input className="inputcheck" type="checkbox" onClick={()=>ocultarEdificio(true)}/>
                    }
                </label>

                <label className="punto">
                    <span className="spanimg"><img className="imagen" alt="Unidades Febriles" src="https://badesdeadentro.gob.ar/sites/default/files/2020-05/ufus.png"/></span><span className="textoinput">Unidades Febriles</span>
                    {
                        mostrarUnidadesFebriles 
                        ?<input className="inputcheck" type="checkbox" checked onClick={()=>ocultarUnidadesFebriles(false)}/>
                        : <input className="inputcheck" type="checkbox" onClick={()=>ocultarUnidadesFebriles(true)}/>
                    }
                </label>
            

                <label className="punto">
                    <span className="spanimg"><img className="imagen" alt="CESAC" src="https://badesdeadentro.gob.ar/sites/default/files/2020-05/atencioncomunitaria.png"/></span><span className="textoinput">CESAC</span>
                    {
                        mostrarCesac
                        ?<input className="inputcheck" type="checkbox" checked onClick={()=>ocultarCesac(false)}/>
                        : <input className="inputcheck" type="checkbox" onClick={()=>ocultarCesac(true)}/> 
                    }
                </label>

                <label className="punto">
                    <span className="spanimg"><img className="imagen" alt="Edificios GCBA" src="https://badesdeadentro.gob.ar/sites/default/files/2020-05/edificiosgcba.png"/></span><span className="textoinput">Edificios GCBA</span>
                    {
                        mostrarEdificioGCBA 
                        ?<input className="inputcheck" type="checkbox" checked onClick={()=>ocultarEdificioGCBA(false)}/>
                        : <input className="inputcheck" type="checkbox" onClick={()=>ocultarEdificioGCBA(true)}/>
                    }
                </label>

                <label className="punto">
                    <span className="spanimg"><img className="imagen" alt="Vacunatorio" src="https://badesdeadentro.gob.ar/sites/default/files/2020-05/VacunatoriosAdultosMayores.png"/></span><span className="textoinput">Vacunatorios</span>
                    {
                        mostrarVacunatorios 
                        ?<input className="inputcheck" type="checkbox" checked onClick={()=>ocultarVacunatorios(false)}/>
                        : <input className="inputcheck" type="checkbox" onClick={()=>ocultarVacunatorios(true)}/>
                    }
                </label>
                <div className="botones">
            {mapa
            ?<img class="botonimg" alt="mapa oscuro" src="https://badesdeadentro.gob.ar/sites/default/files/2020-05/2020-05-17_11h19_32.png" onClick={()=>cambiarMapa(false)} />
            :<img class="botonimg" alt="mapa claro" src="https://badesdeadentro.gob.ar/sites/default/files/2020-05/2020-05-17_11h19_08.png" onClick={()=>cambiarMapa(true)} />
            } 
            </div>
            
            
            
        </Fragment>
     );
}
 
export default Seleccione;