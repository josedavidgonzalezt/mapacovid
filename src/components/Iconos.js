import {Icon} from 'leaflet';

export const icohospital = new Icon({
    iconUrl: 'https://badesdeadentro.gob.ar/sites/default/files/2020-05/Hospitales.png',
    iconSize: [30, 30]
  })

export const icoUfu = new Icon({
    iconUrl: 'https://badesdeadentro.gob.ar/sites/default/files/2020-05/ufus.png',
    iconAnchor: [5, 5],
    popupAnchor: [10, -44],
    iconSize: [25, 25]
  })

export const icoComunitarios = new Icon({
    iconUrl: 'https://badesdeadentro.gob.ar/sites/default/files/2020-05/atencioncomunitaria.png',
    iconSize: [25, 25]
  })

export const icoEdificiosGCBA = new Icon({
      iconUrl: 'https://badesdeadentro.gob.ar/sites/default/files/2020-05/edificiosgcba.png',
      iconSize: [25,25]
  })

export const icoVacunatorios = new Icon({
    iconUrl: 'https://badesdeadentro.gob.ar/sites/default/files/2020-05/VacunatoriosAdultosMayores.png',
    iconSize: [25,25]
})
