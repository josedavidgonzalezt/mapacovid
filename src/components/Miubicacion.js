import LocateControl from './LocateControl';
import React from 'react';

const Miubicacion = () => {
            const locateOptions = {
              strings: {
                  title: 'Mi ubicación!'
              },
              onActivate: () => {} // callback before engine starts retrieving locations
          }
        
        
            return (
                
                 <LocateControl options={locateOptions} startDirectly/>
            );
          }
 
export default Miubicacion;