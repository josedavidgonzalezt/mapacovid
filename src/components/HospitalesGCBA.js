import React, {Fragment,useState} from 'react';
import {Marker,Popup} from 'react-leaflet';
import {icohospital} from './Iconos';
import * as Hospitales from '../data/hospitalesgcba.json';

const HospitalesGCBA = () => {
    //Activa o desactiva el Popup
    const [activePointHostipales, setActivePointHospitales] = useState(null);
    return ( 
        <Fragment>
            {Hospitales.features.map(hospital=>(
                <Marker
                    icon={icohospital} 
                    key={hospital.properties.ID}
                    position={[
                        hospital.geometry.coordinates[1],
                        hospital.geometry.coordinates[0]
                    ]}
                    onclick={()=>{ setActivePointHospitales(hospital);}}
                />
            ))}
            {activePointHostipales && 
                <Popup 
                    position={[
                        activePointHostipales.geometry.coordinates[1],
                        activePointHostipales.geometry.coordinates[0]
                    ]}
                    onClose={()=>{setActivePointHospitales(null);}}
                    >
                    
                    <div className="mypopup">
                        <h5>{activePointHostipales.properties.NOMBRE}</h5>
                        <p>{activePointHostipales.properties.TIPO}</p>
                        <p>Especialidad: {activePointHostipales.properties.TIPO_ESPEC}</p>
                        <p>Dependencia: {activePointHostipales.properties.DEPEND_ADM}</p>
                        <p>Dirección: {activePointHostipales.properties.CALLE} {activePointHostipales.properties.ALTURA}</p>
                    </div>
                </Popup>
            }
        </Fragment>
     );
}
 
export default HospitalesGCBA;