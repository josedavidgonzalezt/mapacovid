import React, {Fragment,useState} from 'react';
import {Marker, Popup} from 'react-leaflet';
import {icoComunitarios} from './Iconos';
import * as Comunitarios from '../data/centrosdesaludyaccioncomunitaria.json';

const Cesac = () => {
    //Activa o desactiva el Popup
    const [activePointComunitaria, setActivePointComunitaria] = useState(null);
    
    return ( 
    <Fragment>

            {Comunitarios.features.map(comunitario=>(
                <Marker
                    icon={icoComunitarios} 
                    key={comunitario.properties.id}
                    position={[
                        comunitario.geometry.coordinates[1],
                        comunitario.geometry.coordinates[0]
                    ]}
                    onclick={()=>{ setActivePointComunitaria(comunitario);}}
                />
            ))}
            {activePointComunitaria && 
                <Popup 
                    position={[
                        activePointComunitaria.geometry.coordinates[1],
                        activePointComunitaria.geometry.coordinates[0]
                    ]}
                    onClose={()=>{setActivePointComunitaria(null);}}
                    >
                    
                    <div className="mypopup">
                        <h5>{activePointComunitaria.properties.nom_ante}</h5>
                        <p>Barrio: {activePointComunitaria.properties.BARRIO}</p>
                        <p>Dirección: {activePointComunitaria.properties.direccion} {activePointComunitaria.properties.altura}</p>
                    </div>
                </Popup>
            }

    </Fragment>


     );
}
 
export default Cesac;